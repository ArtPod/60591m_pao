<?php
echo '<h2>Путь запроса, переданный в точку входа:</h2>';
echo $_GET['path'];
use Framework\Routing\Request;
use Framework\Routing\Router;

require_once "vendor/autoload.php";
echo (new Router(new Request()))->getContent();